Gem::Specification.new do |s|
  s.name        = 'riotic'
  s.version     = '0.0.1'
  s.date        = '2015-02-06'
  s.summary     = 'Riot'
  s.description = 'Riot API'
  s.authors     = ['Rui Miguel']
  s.email       = 'ravage@fragmentized.net'
  s.files       = Dir['lib/*.rb']
  s.homepage    = 'http://rubygems.org/gems/riotic'
  s.license     = 'MIT'

  s.add_runtime_dependency('virtus')
  s.add_runtime_dependency('faraday')
  s.add_runtime_dependency('excon')
  s.add_runtime_dependency('oj')
  s.add_runtime_dependency('multi_json')
end
