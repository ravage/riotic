require 'redis'

module Riotic
  class RedisStore < Store
    def initialize(options = {})
      @options = options.dup

      @store = Redis.new(options)
    end

    def get(key)
      @store.get(key)
    end

    def set(key, value, options = {})
      if options.include?(:ttl)
        @store.setex(key, options.fetch(:ttl), value)
      else
        @store.set(key, value)
      end
    end

    def delete(*keys)
      @store.delete(keys)
    end
  end
end
