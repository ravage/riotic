module Riotic
  class Store

    def get(key)
      raise NotImplementedError
    end

    def set(key, value, options = {})
      raise NotImplementedError
    end

    def delete(*key)
      raise NotImplementedError
    end

  end
end
