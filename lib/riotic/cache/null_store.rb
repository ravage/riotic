module Riotic
  class NullStore < Store
    def get(key); end
    def set(key, value, options = {}); end
    def delete(*key); end
  end
end
