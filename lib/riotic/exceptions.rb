module Riotic
  class BadRequest < StandardError
    def message
      '400: Bad Request'
    end
  end

  class Unauthorized < StandardError
    def message
      '401: Unauthorized'
    end
  end

  class NotFound < StandardError
    def message
      '404: Not Found'
    end
  end

  class RateLimitExceded < StandardError
    def message
      '429: Rate Limit Exceded'
    end
  end

  class InternalServerError < StandardError
    def message
      '500: Internal Server Error'
    end
  end

  class ServiceUnavailable < StandardError
    def message
      '503: Service Unavailable'
    end
  end
end
