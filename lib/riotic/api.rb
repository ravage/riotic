module Riotic
  class API
    def initialize(api_key, region = 'euw', cache = NullStore.new)
      @api_key = api_key
      @region = region
      @cache = cache
    end

    def summoner
      @summoner ||= SummonerRequest.new(@api_key, @region, @cache)
    end

    def match
      @match ||= MatchRequest.new(@api_key, @region, @cache)
    end

    def stats
      @stats ||= StatsRequest.new(@api_key, @region, @cache)
    end

    def game
      @game ||= GameRequest.new(@api_key, @region, @cache)
    end

    def league
      @league ||= LeagueRequest.new(@api_key, @region, @cache)
    end
  end
end
