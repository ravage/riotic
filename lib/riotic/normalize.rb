module Riotic
  module Normalize
    def initialize(attributes = {})
      normalized = {}
      attributes.each { |k, v| normalized[k.to_s.underscore] = v }
      super(normalized)
    end
  end
end
