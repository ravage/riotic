module Riotic
  class Summoner
    include Virtus.model
    include Normalize

    attribute :id, Fixnum
    attribute :name, String
    attribute :profile_icon_id, Fixnum
    attribute :revision_date, Fixnum
    attribute :summoner_level, Fixnum
  end
end
