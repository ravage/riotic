module Riotic
  class MiniSerie
    include Virtus.model

    attribute :losses, Fixnum
    attribute :progress, String
    attribute :target, Fixnum
    attribute :wins, Fixnum
  end

  class LeagueEntry
    include Virtus.model
    include Normalize

    attribute :division, String
    attribute :is_fresh_blood, Boolean
    attribute :is_hot_streak, Boolean
    attribute :is_inactive, Boolean
    attribute :is_veteran, Boolean
    attribute :league_points, Fixnum
    attribute :losses, Fixnum
    attribute :mini_series, MiniSerie
    attribute :player_or_team_id, String
    attribute :player_or_team_name, String
    attribute :wins, Fixnum
  end

  class League
    include Virtus.model
    include Normalize

    attribute :entries, Array[LeagueEntry]
    attribute :name, String
    attribute :participant_id, String
    attribute :queue, String
    attribute :tier, String
  end
end