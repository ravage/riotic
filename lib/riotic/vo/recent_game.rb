module Riotic
  class Player
    include Virtus.model
    include Normalize

    attribute :champion_id, Fixnum
    attribute :summoner_id, Fixnum
    attribute :team_id, Fixnum
  end

  class GameStats
    include Virtus.model
    include Normalize

    attribute :assists, Fixnum
    attribute :barracks_killed, Fixnum
    attribute :champions_killed, Fixnum
    attribute :combat_player_score, Fixnum
    attribute :consumables_purchased, Fixnum
    attribute :damage_dealt_player, Fixnum
    attribute :double_kills, Fixnum
    attribute :first_blood, Fixnum
    attribute :gold, Fixnum
    attribute :gold_earned, Fixnum
    attribute :gold_spent,  Fixnum
    attribute :item0, Fixnum
    attribute :item1, Fixnum
    attribute :item2, Fixnum
    attribute :item3, Fixnum
    attribute :item4, Fixnum
    attribute :item5, Fixnum
    attribute :item6, Fixnum
    attribute :items_purchased, Fixnum
    attribute :killing_sprees, Fixnum
    attribute :largest_critical_strike, Fixnum
    attribute :largest_killing_spree, Fixnum
    attribute :largest_multi_kill, Fixnum
    attribute :legendary_items_created, Fixnum
    attribute :level, Fixnum
    attribute :magic_damage_dealt_player, Fixnum
    attribute :magic_damage_dealt_to_champions, Fixnum
    attribute :magic_damage_taken, Fixnum
    attribute :minions_denied, Fixnum
    attribute :minions_killed, Fixnum
    attribute :neutral_minions_killed, Fixnum
    attribute :neutral_minions_killed_enemy_jungle, Fixnum
    attribute :neutral_minions_killed_your_jungle, Fixnum
    attribute :nexus_killed
    attribute :node_capture, Fixnum
    attribute :node_capture_assist, Fixnum
    attribute :node_neutralize, Fixnum
    attribute :node_neutralize_assist, Fixnum
    attribute :num_deaths, Fixnum
    attribute :num_items_bought, Fixnum
    attribute :objective_player_score, Fixnum
    attribute :penta_kills, Fixnum
    attribute :physical_damage_dealt_player,  Fixnum
    attribute :physical_damage_dealt_to_champions, Fixnum
    attribute :physical_damage_taken,  Fixnum
    attribute :player_position, Fixnum
    attribute :player_role, Fixnum
    attribute :quadra_kills,  Fixnum
    attribute :sight_wards_bought, Fixnum
    attribute :spell1_cast, Fixnum
    attribute :spell2_cast, Fixnum
    attribute :spell3_cast, Fixnum
    attribute :spell4_cast, Fixnum
    attribute :summon_spell1_cast, Fixnum
    attribute :summon_spell2_cast, Fixnum
    attribute :super_monster_killed, Fixnum
    attribute :team, Fixnum
    attribute :team_objective,  Fixnum
    attribute :time_played, Fixnum
    attribute :total_damage_dealt, Fixnum
    attribute :total_damage_dealt_to_champions,  Fixnum
    attribute :total_damage_taken, Fixnum
    attribute :total_heal, Fixnum
    attribute :total_player_score, Fixnum
    attribute :total_score_rank, Fixnum
    attribute :total_time_crowd_control_dealt, Fixnum
    attribute :total_units_healed, Fixnum
    attribute :triple_kills, Fixnum
    attribute :true_damage_dealt_player, Fixnum
    attribute :true_damage_dealt_to_champions, Fixnum
    attribute :true_damage_taken, Fixnum
    attribute :turrets_killed, Fixnum
    attribute :unreal_kills, Fixnum
    attribute :victory_po_fixnum_total, Fixnum
    attribute :vision_wards_bought, Fixnum
    attribute :ward_killed, Fixnum
    attribute :ward_placed, Fixnum
    attribute :win, Boolean

  end

  class Game
    include Virtus.model
    include Normalize

    attribute :champion_id, Fixnum
    attribute :create_date, Fixnum
    attribute :fellow_players, Array[Player]
    attribute :game_id, Fixnum
    attribute :game_mode, String
    attribute :game_type, String
    attribute :invalid, Boolean
    attribute :ip_earned, Fixnum
    attribute :level, Fixnum
    attribute :map_id, Fixnum
    attribute :spell1, Fixnum
    attribute :spell2, Fixnum
    attribute :stats, GameStats
    attribute :sub_type, String
    attribute :team_id, Fixnum

  end

  class RecentGame
    include Virtus.model
    include Normalize

    attribute :games, Array[Game]
    attribute :summoner_id, Fixnum
  end
end
