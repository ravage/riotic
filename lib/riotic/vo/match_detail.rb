module Riotic
  class Position
    include Virtus.model
    include Normalize

    attribute :x, Fixnum
    attribute :y, Fixnum
  end

  class ParticipantFrame
    include Virtus.model
    include Normalize

    attribute :current_gold, Fixnum
    attribute :dominion_score, Fixnum
    attribute :jungle_minions_killed, Fixnum
    attribute :level, Fixnum
    attribute :minions_killed, Fixnum
    attribute :participant_id, Fixnum
    attribute :position, Position
    attribute :team_score, Fixnum
    attribute :total_gold, Fixnum
    attribute :xp, Fixnum
  end

  class Event
    include Virtus.model
    include Normalize

    attribute :ascended_type, String
    attribute :assisting_participant_ids, Array
    attribute :building_type, String
    attribute :creator_id, Fixnum
    attribute :event_type, String
    attribute :item_after, Fixnum
    attribute :item_before, Fixnum
    attribute :item_id, Fixnum
    attribute :killer_id, Fixnum
    attribute :lane_type, String
    attribute :level_up_type, String
    attribute :monster_type, String
    attribute :participant_id, Fixnum
    attribute :point_captured, String
    attribute :position, Position
    attribute :skill_slot, Fixnum
    attribute :team_id, Fixnum
    attribute :timestamp, Fixnum
    attribute :tower_type, String
    attribute :victim_id , Fixnum
    attribute :ward_type , String
  end


  class ParticipantTimelineData
    include Virtus.model
    include Normalize

    attribute :ten_to_twenty, Float
    attribute :thirty_to_end, Float
    attribute :twenty_to_thirty, Float
    attribute :zero_to_ten, Float
  end

  class Frame
    include Virtus.model
    include Normalize

    attribute :events, Array[Event]
    attribute :participant_frames, Hash[String => ParticipantFrame]
    attribute :timestamp, Time
  end

  class BannedChampion
    include Virtus.model
    include Normalize

    attribute :champion_id, Fixnum
    attribute :pick_turn, Fixnum
  end

  class Player
    include Virtus.model
    include Normalize

    attribute :match_history_uri, String
    attribute :profile_icon, Fixnum
    attribute :summoner_id, Fixnum
    attribute :summoner_name, String
  end

  class ParticipantTimeline
    include Virtus.model
    include Normalize

    attribute :ancient_golem_assists_per_min_counts, ParticipantTimelineData
    attribute :ancient_golem_kills_per_min_counts, ParticipantTimelineData
    attribute :assisted_lane_deaths_per_min_deltas, ParticipantTimelineData
    attribute :assisted_lane_kills_per_min_deltas, ParticipantTimelineData
    attribute :baron_assists_per_min_counts, ParticipantTimelineData
    attribute :baron_kills_per_min_counts, ParticipantTimelineData
    attribute :creeps_per_min_deltas, ParticipantTimelineData
    attribute :cs_diff_per_min_deltas, ParticipantTimelineData
    attribute :damage_taken_diff_per_min_deltas, ParticipantTimelineData
    attribute :damage_taken_per_min_deltas, ParticipantTimelineData
    attribute :dragon_assists_per_min_counts, ParticipantTimelineData
    attribute :dragon_kills_per_min_counts, ParticipantTimelineData
    attribute :elder_lizard_assists_per_min_counts, ParticipantTimelineData
    attribute :elder_lizard_kills_per_min_counts, ParticipantTimelineData
    attribute :gold_per_min_deltas, ParticipantTimelineData
    attribute :inhibitor_assists_per_min_counts, ParticipantTimelineData
    attribute :inhibitor_kills_per_min_counts,  ParticipantTimelineData
    attribute :lane, String
    attribute :role, String
    attribute :tower_assists_per_min_counts, ParticipantTimelineData
    attribute :tower_kills_per_min_counts, ParticipantTimelineData
    attribute :tower_kills_per_min_deltas, ParticipantTimelineData
    attribute :vilemaw_assists_per_min_counts, ParticipantTimelineData
    attribute :vilemaw_kills_per_min_counts, ParticipantTimelineData
    attribute :wards_per_min_deltas, ParticipantTimelineData
    attribute :xp_diff_per_min_deltas, ParticipantTimelineData
    attribute :xp_per_min_deltas, ParticipantTimelineData
  end

  class ParticipantStats
    include Virtus.model
    include Normalize

    attribute :assists, Fixnum
    attribute :champ_level, Fixnum
    attribute :combat_player_score, Fixnum
    attribute :deaths, Fixnum
    attribute :double_kills, Fixnum
    attribute :first_blood_assist, Boolean
    attribute :first_blood_kill, Boolean
    attribute :first_inhibitor_assist, Boolean
    attribute :first_inhibitor_kill, Boolean
    attribute :first_tower_assist, Boolean
    attribute :first_tower_kill, Boolean
    attribute :gold_earned, Fixnum
    attribute :gold_spent, Fixnum
    attribute :inhibitor_kills, Fixnum
    attribute :item0, Fixnum
    attribute :item1, Fixnum
    attribute :item2, Fixnum
    attribute :item3, Fixnum
    attribute :item4, Fixnum
    attribute :item5, Fixnum
    attribute :item6, Fixnum
    attribute :killing_sprees, Fixnum
    attribute :kills, Fixnum
    attribute :largest_critical_strike, Fixnum
    attribute :largest_killing_spree, Fixnum
    attribute :largest_multi_kill, Fixnum
    attribute :magic_damage_dealt, Fixnum
    attribute :magic_damage_dealt_to_champions, Fixnum
    attribute :magic_damage_taken, Fixnum
    attribute :minions_killed, Fixnum
    attribute :neutral_minions_killed, Fixnum
    attribute :neutral_minions_killed_enemy_jungle, Fixnum
    attribute :neutral_minions_killed_team_jungle, Fixnum
    attribute :node_capture, Fixnum
    attribute :node_capture_assist, Fixnum
    attribute :node_neutralize, Fixnum
    attribute :node_neutralize_assist, Fixnum
    attribute :objective_player_score, Fixnum
    attribute :penta_kills, Fixnum
    attribute :physical_damage_dealt, Fixnum
    attribute :physical_damage_dealt_to_champions, Fixnum
    attribute :physical_damage_taken, Fixnum
    attribute :quadra_kills, Fixnum
    attribute :sight_wards_bought_in_game, Fixnum
    attribute :team_objective, Fixnum
    attribute :total_damage_dealt, Fixnum
    attribute :total_damage_dealt_to_champions, Fixnum
    attribute :total_damage_taken, Fixnum
    attribute :total_heal, Fixnum
    attribute :total_player_score, Fixnum
    attribute :total_score_rank, Fixnum
    attribute :total_time_crowd_control_dealt, Fixnum
    attribute :total_units_healed, Fixnum
    attribute :tower_kills, Fixnum
    attribute :tripleKills, Fixnum
    attribute :true_damage_dealt, Fixnum
    attribute :true_damage_dealt_to_champions, Fixnum
    attribute :true_damage_taken, Fixnum
    attribute :unreal_kills, Fixnum
    attribute :vision_wards_bought_in_game, Fixnum
    attribute :wards_killed, Fixnum
    attribute :wards_placed, Fixnum
    attribute :winner, Boolean
  end


  class Timeline
    include Virtus.model
    include Normalize

    attribute :frame_interval, Fixnum
    attribute :frames, Array[Frame]
  end

  class Team
    include Virtus.model
    include Normalize

    attribute :bans, Array[BannedChampion]
    attribute :baron_kills, Fixnum
    attribute :dominion_victory_score, Fixnum
    attribute :dragon_kills, Fixnum
    attribute :first_baron, Boolean
    attribute :first_blood, Boolean
    attribute :first_dragon, Boolean
    attribute :first_inhibitor, Boolean
    attribute :first_tower, Boolean
    attribute :inhibitor_kills,  Fixnum
    attribute :team_id, Fixnum
    attribute :tower_kills, Fixnum
    attribute :vilemaw_kills, Fixnum
    attribute :winner, Boolean
  end

  class ParticipantIdentity
    include Virtus.model
    include Normalize

    attribute :participant_id, Fixnum
    attribute :player, Player
  end

  class Participant
    include Virtus.model
    include Normalize

    attribute :champion_id, Fixnum
    attribute :highest_achieved_season_tier, String
    attribute :masteries, Array[Mastery]
    attribute :participant_id, Fixnum
    attribute :runes, Array[Rune]
    attribute :spell1_id, Fixnum
    attribute :spell2_id, Fixnum
    attribute :stats, ParticipantStats
    attribute :team_id, Fixnum
    attribute :timeline, ParticipantTimeline
  end

  class MatchDetail
    include Virtus.model
    include Normalize

    attribute :map_id, Fixnum
    attribute :match_creation, Fixnum
    attribute :match_duration, Fixnum
    attribute :match_id, Fixnum
    attribute :match_mode, String
    attribute :match_type, String
    attribute :match_version, String
    attribute :participant_identities, Array[ParticipantIdentity]
    attribute :participants, Array[Participant]
    attribute :platform_id, String
    attribute :queue_type, String
    attribute :region, String
    attribute :season, String
    attribute :teams, Array[Team]
    attribute :timeline, Timeline
  end
end
