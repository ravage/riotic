module Riotic

  class MatchSummary
    include Virtus.model
    include Normalize

    attribute :map_id, Fixnum
    attribute :match_creation, Fixnum
    attribute :match_duration, Fixnum
    attribute :match_id, Fixnum
    attribute :match_mode, String
    attribute :match_type, String
    attribute :match_version, String
    attribute :participant_identities, Array[ParticipantIdentity]
    attribute :participants,  Array[Participant]
    attribute :platform_id, String
    attribute :queue_type, String
    attribute :region, String
    attribute :season, String
  end

  class MatchHistory
    include Virtus.model

    attribute :matches, Array[MatchSummary]
  end
end