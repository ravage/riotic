module Riotic
  class AggregatedStats
    include Virtus.model
    include Normalize

    attribute :average_assists, Fixnum
    attribute :average_champions_killed, Fixnum
    attribute :average_combat_player_score, Fixnum
    attribute :average_node_capture, Fixnum
    attribute :average_node_capture_assist, Fixnum
    attribute :average_node_neutralize, Fixnum
    attribute :average_node_neutralize_assist, Fixnum
    attribute :average_num_deaths, Fixnum
    attribute :average_objective_player_score, Fixnum
    attribute :average_team_objective, Fixnum
    attribute :average_total_player_score, Fixnum
    attribute :bot_games_played, Fixnum
    attribute :killing_spree, Fixnum
    attribute :max_assists, Fixnum
    attribute :max_champions_killed, Fixnum
    attribute :max_combat_player_score, Fixnum
    attribute :max_largest_critical_strike, Fixnum
    attribute :max_largest_killing_spree, Fixnum
    attribute :max_node_capture, Fixnum
    attribute :max_node_capture_assist, Fixnum
    attribute :max_node_neutralize, Fixnum
    attribute :max_node_neutralize_assist, Fixnum
    attribute :max_num_deaths, Fixnum
    attribute :max_objective_player_score, Fixnum
    attribute :max_team_objective, Fixnum
    attribute :max_time_played, Fixnum
    attribute :max_time_spent_living, Fixnum
    attribute :max_total_player_score, Fixnum
    attribute :most_champion_kills_per_session, Fixnum
    attribute :most_spells_cast, Fixnum
    attribute :normal_games_played, Fixnum
    attribute :ranked_premade_games_played, Fixnum
    attribute :ranked_solo_games_played, Fixnum
    attribute :total_assists, Fixnum
    attribute :total_champion_kills, Fixnum
    attribute :total_damage_dealt, Fixnum
    attribute :total_damage_taken, Fixnum
    attribute :total_deaths_per_session, Fixnum
    attribute :total_double_kills, Fixnum
    attribute :total_first_blood, Fixnum
    attribute :total_gold_earned, Fixnum
    attribute :total_heal, Fixnum
    attribute :total_magic_damage_dealt, Fixnum
    attribute :total_minion_kills, Fixnum
    attribute :total_neutral_minions_killed, Fixnum
    attribute :total_node_capture, Fixnum
    attribute :total_node_neutralize, Fixnum
    attribute :total_penta_kills, Fixnum
    attribute :total_physical_damage_dealt, Fixnum
    attribute :total_quadra_kills, Fixnum
    attribute :total_sessions_lost, Fixnum
    attribute :total_sessions_played, Fixnum
    attribute :total_sessions_won, Fixnum
    attribute :total_triple_kills, Fixnum
    attribute :total_turrets_killed, Fixnum
    attribute :total_unreal_kills, Fixnum
  end

  class PlayerStatSummary
    include Virtus.model
    include Normalize

    attribute :aggregated_stats, AggregatedStats
    attribute :losses, Fixnum
    attribute :modify_date, Fixnum
    attribute :player_stat_summary_type, String
    attribute :wins, Fixnum
  end

  class Stats
    include Virtus.model
    include Normalize

    attribute :summoner_id, Fixnum
    attribute :player_stat_summaries, Array[PlayerStatSummary]
  end
end
