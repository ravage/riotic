module Riotic

  class Mastery
    include Virtus.model
    include Normalize

    attribute :id, Fixnum
    attribute :rank, Fixnum

    def mastery_id=(id)
      self.id = id
    end
  end

  class MasteryPage
    include Virtus.model

    attribute :id, Fixnum
    attribute :name, String
    attribute :current, Boolean
    attribute :masteries, Array[Mastery]
  end

  class MasteryPageCollection
    include Virtus.model
    include Normalize

    attribute :summoner_id, Fixnum
    attribute :pages, Array[MasteryPage]
  end

end
