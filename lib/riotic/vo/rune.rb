module Riotic
  class Slot
    include Virtus.model
    include Normalize

    attribute :run_slot_id, Fixnum
    attribute :run_id, Fixnum
  end

  class RunePage
    include Virtus.model

    attribute :id, Fixnum
    attribute :name, String
    attribute :current, Boolean
    attribute :slots, Array[Slot]
  end

  class RunePageCollection
    include Virtus.model
    include Normalize

    attribute :summoner_id, Fixnum
    attribute :pages, Array[RunePage]
  end

  class Rune
    include Virtus.model
    include Normalize

    attribute :id, Fixnum
    attribute :rank, Fixnum

    def rune_id=(id)
      self.id = id
    end
  end

end
