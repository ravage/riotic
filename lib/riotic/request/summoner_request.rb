module Riotic
  class SummonerRequest < Request

    ENDPOINTS = {
      by_name:    '/summoner/by-name/|names|',
      by_id:      '/summoner/|ids|',
      masteries:  '/summoner/|ids|/masteries',
      name:       '/summoner/|ids|/name',
      runes:      '/summoner/|ids|/runes'
    }

    VERSION = 'v1.4'

    def by_name(*names)
      names = names.map { |name| escape(name) }
      endpoint = build_endpoint(__method__, '|names|', names.join(','))
      result = get(endpoint)
      result.map { |k, v| Summoner.new(v) }
    end

    def by_id(*ids)
      endpoint = build_endpoint(__method__, '|ids|', ids.join(','))
      result = get(endpoint)
      result.map { |k, v| Summoner.new(v) }
    end

    def masteries(*ids)
      endpoint = build_endpoint(__method__, '|ids|', ids.join(','))
      result = get(endpoint)
      result.values.map { |v| MasteryPageCollection.new(v) }
    end

    def runes(*ids)
      endpoint = build_endpoint(__method__, '|ids|', ids.join(','))
      result = get(endpoint)
      result.values.map { |v| RunePageCollection.new(v) }
    end

    def name(*ids)
      endpoint = build_endpoint(__method__, '|ids|', ids.join(','))
      result = get(endpoint)
      result.map { |k, v| Summoner.new(id: k, name: v) }
    end
  end
end
