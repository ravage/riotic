module Riotic
  class LeagueRequest < Request
    ENDPOINTS = {
      summoner_entry: '/league/by-summoner/|id|/entry'
    }

    VERSION = 'v2.5'

    def summoner_entry(id)
      endpoint = build_endpoint(__method__, '|id|', id.to_s)
      result = get(endpoint)
      result[id.to_s].map { |league| League.new(league) }
    end
  end
end