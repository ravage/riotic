module Riotic
  class GameRequest < Request
    ENDPOINTS = {
      recent: '/game/by-summoner/|id|/recent'
    }

    VERSION = 'v1.3'

    def recent(id)
      endpoint = build_endpoint(__method__, '|id|', id.to_s)
      result = get(endpoint)
      RecentGame.new(result)
    end
  end
end
