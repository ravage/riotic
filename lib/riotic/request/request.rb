module Riotic
  class Request

    ERRORS = {
      400 => BadRequest,
      401 => Unauthorized,
      404 => NotFound,
      429 => RateLimitExceded,
      500 => InternalServerError,
      503 => ServiceUnavailable
    }


    def initialize(api_key, region, cache = NullStore.new)
      @api_key = api_key
      @region = region
      @cache = cache

      url = "https://#{region}.api.pvp.net"

      @conn = Faraday.new(url: url) do |faraday|
        faraday.request  :url_encoded
        faraday.adapter  :excon
      end
    end

    def endpoint_prefix
      "/api/lol/#{@region}/#{version}"
    end

    def build_endpoint(method, placeholder, values)
      endpoint_prefix << endpoints[method].sub(placeholder, values)
    end

    def escape(value)
      CGI.escape value.downcase.gsub(/\s/, '')
    end

    def get(endpoint, query = nil)
      data = @cache.get("riotic:#{endpoint}")

      if data.nil?
        response = @conn.get(endpoint, { api_key: @api_key })

        raise ERRORS.fetch(response.status) if ERRORS.include?(response.status)

        data = response.body

        @cache.set("riotic:#{endpoint}", data, ttl: 900)
      end

      MultiJson.load(data)
    end

    def endpoints
      self.class::ENDPOINTS
    end

    def version
      self.class::VERSION
    end
  end
end
