module Riotic
  class MatchRequest < Request
    ENDPOINTS = {
      match:    '/match/|id|',
      history:  '/matchhistory/|id|'
    }

    VERSION = 'v2.2'

    def match(id)
      endpoint = build_endpoint(__method__, '|id|', id.to_s)
      result = get(endpoint)
      MatchDetail.new(result)
    end

    def history(id)
      endpoint = build_endpoint(__method__, '|id|', id.to_s)
      result = get(endpoint)
      MatchHistory.new(result)
    end
  end
end