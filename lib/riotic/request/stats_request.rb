module Riotic
  class StatsRequest < Request
    ENDPOINTS = {
      ranked:   '/stats/by-summoner/|id|/ranked',
      summary:  '/stats/by-summoner/|id|/summary'
    }

    VERSION = 'v1.3'

    def summary(id)
      endpoint = build_endpoint(__method__, '|id|', id.to_s)
      result = get(endpoint)
      Stats.new(result)
    end

    def ranked(id)
    end
  end
end
