require_relative 'spec_helper'

describe StatsRequest do
  before do
    VCR.insert_cassette 'stats'
  end

  after do
    VCR.eject_cassette
  end

  let(:sr) { StatsRequest.new(ENV['API_KEY'], 'euw', RedisStore.new) }
  let(:id1) { 26557460 }

  describe '#summary' do
    it 'should return a StatsRequest object' do
      result = sr.summary(24279582)
      result.summoner_id.must_equal 24279582
      result.player_stat_summaries.wont_be_empty
    end
  end
end
