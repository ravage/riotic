require 'minitest/spec'
require 'minitest/autorun'
require 'minitest/pride'
require 'pry'

$LOAD_PATH.unshift(File.join(File.dirname(__FILE__)), '..', 'lib')

require 'riotic'

include Riotic

require 'vcr'

VCR.configure do |c|
  c.hook_into :excon
  c.default_cassette_options = { record: :new_episodes }
  c.cassette_library_dir = 'cassettes'
  c.allow_http_connections_when_no_cassette = true
end
