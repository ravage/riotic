require_relative 'spec_helper'

describe Summoner do
  before do
    @summoner = Summoner.new.tap do |o|
      o.id = '1'
      o.name = 'Name'
      o.profile_icon_id = 1
      o.revision_date = 1
      o.summoner_level = 1
    end
  end

  it "initializes attributes" do
    @summoner.id.must_be_kind_of Fixnum
    @summoner.name.must_be_kind_of String
    @summoner.profile_icon_id.must_be_kind_of Fixnum
  end
end
