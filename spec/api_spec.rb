require_relative 'spec_helper'

describe API do
  before do
    VCR.insert_cassette 'summoner'
    @api = API.new(ENV['API_KEY'], 'euw', RedisStore.new)
  end

  after do
    VCR.eject_cassette
  end

  describe '#summoner' do
    it 'returns a SummonerRequest' do
      @api.summoner.must_be_instance_of SummonerRequest
    end

    it 'allow initializes SummonerRequest' do
      @api.summoner.by_name('nilstrung').first.must_be_instance_of Summoner
    end
  end
end
