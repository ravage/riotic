require_relative 'spec_helper'

describe 'Versions' do
  it 'should have a matching api version' do
    SummonerRequest::VERSION.must_equal 'v1.4'
    StatsRequest::VERSION.must_equal 'v1.3'
  end
end
