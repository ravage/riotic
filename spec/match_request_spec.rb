require_relative 'spec_helper'

describe MatchRequest do
  before do
    VCR.insert_cassette 'match'
  end

  after do
    VCR.eject_cassette
  end

  let(:mr) { MatchRequest.new(ENV['API_KEY'], 'euw', RedisStore.new) }
  let(:id) { 1980180122 }
  let(:summoner_id) { 26557460 }

  describe '#match' do
    it 'should return a MatchDetail object' do
      result = mr.match(id).must_be_instance_of MatchDetail
    end
  end

  describe '#history' do
    it 'should return a MatchHistory object' do
      result = mr.history(summoner_id).must_be_instance_of MatchHistory
    end
  end
end