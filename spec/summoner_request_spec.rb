require_relative 'spec_helper'

describe SummonerRequest do
  before do
    VCR.insert_cassette 'summoner'
  end

  after do
    VCR.eject_cassette
  end

  let(:sr) { SummonerRequest.new(ENV['API_KEY'], 'euw', RedisStore.new) }
  let(:id1) { 26557460 }
  let(:id2) { 20521881 }

  describe '#by_name' do
    it 'should return 2 summoners' do
      sr.by_name('nilstrung', 'ravage').length.must_equal 2
    end

    it 'should ignore none existent summoners' do
      sr.by_name('nilstrung', 'ravage', 'nilstrung no no no no').length.must_equal 2
    end

    it 'should return empty array' do
      sr.by_name('nilstrung does not exist').length.must_equal 0
    end

    it 'should return a summoner' do
      sr.by_name('nilstrung').first.must_be_instance_of Summoner
    end

    it 'accepts an array of names' do
      names = ['nilstrung', 'ravage']
      sr.by_name(*names).length.must_equal 2
    end
  end

  describe '#name' do
    it 'should return a maps of names' do
      sr.name(20521881).must_be_instance_of Array
      sr.name(26557460,  20521881).must_be_instance_of Array
    end

    it 'shoud convert map to Summoner' do
      sr.name(20521881).first.must_be_instance_of Summoner
      sr.name(26557460,  20521881).each do |v|
        v.must_be_instance_of Summoner
      end
    end

    it 'must fill id and name attributes' do
      summoners = sr.name(20521881, 26557460)

      summoners.last.id.must_equal 20521881
      summoners.last.name.must_equal 'Ravage'

      summoners.first.id.must_equal 26557460
      summoners.first.name.must_equal 'nilstrung'
    end
  end

  describe '#by_id' do
    it 'should return 2 summoners' do
      sr.by_id(id1, id2).length.must_equal 2
    end

    it 'should return ignore none existent summoners' do
      sr.by_id(id1, id2, 00000000).length.must_equal 2
    end

    it 'should return empty array' do
      sr.by_id(00000000).length.must_equal 0
    end

    it 'should return a summoner' do
      sr.by_id(id1).first.must_be_instance_of Summoner
    end

    it 'should populate Summoner attributes' do
      summoner = sr.by_id(id1).first
      summoner.id.must_equal id1
      summoner.name.must_equal 'nilstrung'
    end
  end

  describe '#masteries' do
    it 'shoult return 1 mastery' do
      masteries = sr.masteries(id1)
      masteries.wont_be_empty
      masteries.first.summoner_id.must_equal id1
      masteries.first.pages.wont_be_empty
      masteries.first.pages.first.masteries.wont_be_empty
    end

    it 'should return more than one mastery' do
      masteries = sr.masteries(id1, id2)
      masteries.wont_be_empty
      masteries.first.summoner_id.must_equal id1
      masteries.last.summoner_id.must_equal id2
    end
  end

  describe '#runes' do
    it 'shoult return 1 rune' do
      runes = sr.runes(id1)
      runes.wont_be_empty
      runes.first.summoner_id.must_equal id1
      runes.first.pages.wont_be_empty
      runes.first.pages.first.slots.wont_be_empty
    end

    it 'should return more than one rune' do
      runes = sr.runes(id1, id2)
      runes.wont_be_empty
      runes.first.summoner_id.must_equal id1
      runes.last.summoner_id.must_equal id2
    end
  end
end
